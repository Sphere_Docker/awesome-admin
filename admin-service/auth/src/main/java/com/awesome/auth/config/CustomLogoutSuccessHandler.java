package com.awesome.auth.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**  
 *                     .::::.
 *                   .::::::::.
 *                  :::::::::::    佛主保佑、永无Bug
 *              ..:::::::::::'
 *            '::::::::::::'
 *              .::::::::::
 *         '::::::::::::::..
 *              ..::::::::::::.
 *           ``::::::::::::::::
 *             ::::``:::::::::'        .:::.
 *            ::::'   ':::::'       .::::::::.
 *         .::::'      ::::     .:::::::'::::.
 *        .:::'       :::::  .:::::::::' ':::::.
 *       .::'        :::::.:::::::::'      ':::::.
 *      .::'         ::::::::::::::'         ``::::.
 *  ...:::           ::::::::::::'              ``::.
 * ```` ':.          ':::::::::'                  ::::..
 *                   '.:::::'                    ':'````..
 * @version: V1.0  
 * @author: qxw  
 * @description: (登出成功处理器)登出成功，不跳转页面，返回自定义返回值
 * @data: 2020年04月11日 15:09
 * @ModifyDate: 2020年04月11日 15:09 
 **/ 
@Component
public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {
    
    private static final Logger LOG = LoggerFactory.getLogger(CustomLogoutSuccessHandler.class);

    @Override
    public void onLogoutSuccess(
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse,
            Authentication authentication) throws IOException {

        //在页面进行授权，跳转到redirect_uri，并带上了授权码
        String redirectUrl = httpServletRequest.getParameter("redirect_url");
        httpServletRequest.getSession().invalidate();
        httpServletResponse.sendRedirect(redirectUrl);
        LOG.info("❤❤❤❤❤❤❤❤❤SpringSecurityConfig中触发-自定义的 LogoutSuccessHandler❤❤❤❤❤❤❤❤❤"+redirectUrl);
    }
}
