package com.awesome.auth.service.authentication;

import com.awesome.auth.service.interfaces.UsernamePasswordUserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;

/***
 *                     .::::.
 *                   .::::::::.
 *                  :::::::::::    佛主保佑、永无Bug
 *              ..:::::::::::'
 *            '::::::::::::'
 *              .::::::::::
 *         '::::::::::::::..
 *              ..::::::::::::.
 *           ``::::::::::::::::
 *             ::::``:::::::::'        .:::.
 *            ::::'   ':::::'       .::::::::.
 *         .::::'      ::::     .:::::::'::::.
 *        .:::'       :::::  .:::::::::' ':::::.
 *       .::'        :::::.:::::::::'      ':::::.
 *      .::'         ::::::::::::::'         ``::::.
 *  ...:::           ::::::::::::'              ``::.
 * ```` ':.          ':::::::::'                  ::::..
 *                   '.:::::'                    ':'````..
 * @version: V1.0
 * @author: qxw
 * @description: 用户名密码身份验证提供程序
 * @data: 2020年04月10日 17:39
 * @ModifyDate: 2020年04月10日 17:39
 **/
public class UsernamePasswordAuthenticationProvider implements AuthenticationProvider {

    private static final Logger LOG = LoggerFactory.getLogger(UsernamePasswordAuthenticationProvider.class);

    private UsernamePasswordUserDetailService userDetailService;

    public UsernamePasswordAuthenticationProvider(UsernamePasswordUserDetailService userDetailService) {

        LOG.info("㊁实现UsernamePasswordAuthenticationProvider实现authenticate和supports方法用于用户认证");

        this.userDetailService = userDetailService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        // 验证用户名和密码
        if (userDetailService.verifyCredential(username, password)) {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("user"));
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password, authorities);
            token.setDetails(userDetailService.getUserDetail(username));
            return token;
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        // 用户名和密码登录时，使用该provider认证
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(aClass));
    }
}
