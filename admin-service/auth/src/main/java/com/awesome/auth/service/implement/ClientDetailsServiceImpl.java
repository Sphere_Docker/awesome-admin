package com.awesome.auth.service.implement;

import com.awesome.auth.model.app.AppCredential;
import com.awesome.auth.model.app.AppCredentialDetail;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.stereotype.Service;

/**  
 *                     .::::.
 *                   .::::::::.
 *                  :::::::::::    佛主保佑、永无Bug
 *              ..:::::::::::'
 *            '::::::::::::'
 *              .::::::::::
 *         '::::::::::::::..
 *              ..::::::::::::.
 *           ``::::::::::::::::
 *             ::::``:::::::::'        .:::.
 *            ::::'   ':::::'       .::::::::.
 *         .::::'      ::::     .:::::::'::::.
 *        .:::'       :::::  .:::::::::' ':::::.
 *       .::'        :::::.:::::::::'      ':::::.
 *      .::'         ::::::::::::::'         ``::::.
 *  ...:::           ::::::::::::'              ``::.
 * ```` ':.          ':::::::::'                  ::::..
 *                   '.:::::'                    ':'````..
 * @version: V1.0  
 * @author: qxw  
 * @description: 自定义客户端数据（读取的是第三方应用的信息）
 * ㊁先实现ClientDetails 然后再实现 ClientDetailsService
 * @data: 2020年04月11日 17:17
 * @ModifyDate: 2020年04月11日 17:17 
 **/ 
@Service
@Primary
public class ClientDetailsServiceImpl implements ClientDetailsService {


    @Override
    public ClientDetails loadClientByClientId(String s) {

        if ("weixin".equals(s) || "app".equals(s) || "mini_app".equals(s) || "global".equals(s)) {
            AppCredential credential = new AppCredential();
            credential.setAppId(s);
            credential.setAppSecret("testpassword");
            credential.setGrantTypes("authorization_code,client_credentials,password,refresh_token,mini_app");
            credential.setAccessExpireTime(3600 * 24);
            credential.setRefreshExpireTime(3600 * 24 * 30);
            credential.setRedirectUrl("http://localhost:3006,http://localhost:3006/auth,http://localhost:8000,http://localhost:8000/auth,http://admin.awesome-coder.com/auth");
            credential.setScopes("all");
            //ClientDetailsSevice读取到的信息都会封装到ClientDetails这个对象中
            return new AppCredentialDetail(credential);
        }
        return null;
    }
}
