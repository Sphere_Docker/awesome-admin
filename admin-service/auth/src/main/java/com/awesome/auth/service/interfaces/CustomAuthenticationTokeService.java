package com.awesome.auth.service.interfaces;

import com.awesome.auth.service.authentication.CustomAuthenticationToken;

import java.util.Map;

/**
 *                     .::::.
 *                   .::::::::.
 *                  :::::::::::    佛主保佑、永无Bug
 *              ..:::::::::::'
 *            '::::::::::::'
 *              .::::::::::
 *         '::::::::::::::..
 *              ..::::::::::::.
 *           ``::::::::::::::::
 *             ::::``:::::::::'        .:::.
 *            ::::'   ':::::'       .::::::::.
 *         .::::'      ::::     .:::::::'::::.
 *        .:::'       :::::  .:::::::::' ':::::.
 *       .::'        :::::.:::::::::'      ':::::.
 *      .::'         ::::::::::::::'         ``::::.
 *  ...:::           ::::::::::::'              ``::.
 * ```` ':.          ':::::::::'                  ::::..
 *                   '.:::::'                    ':'````..
 * @version: V1.0
 * @author: qxw
 * @description: 自定义身份验证令牌接口
 * @data: 2020年04月11日 18:30
 * @ModifyDate: 2020年04月11日 18:30
 **/
public interface CustomAuthenticationTokeService {

    /**
     * 获取 authentication token
     * @param clientId appId
     * @param params params
     * @return token
     */
    CustomAuthenticationToken createAuthenticationToken(String clientId, Map<String, String> params);
}
