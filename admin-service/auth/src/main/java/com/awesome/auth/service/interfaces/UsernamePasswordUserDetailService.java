package com.awesome.auth.service.interfaces;

/**
 *                     .::::.
 *                   .::::::::.
 *                  :::::::::::    佛主保佑、永无Bug
 *              ..:::::::::::'
 *            '::::::::::::'
 *              .::::::::::
 *         '::::::::::::::..
 *              ..::::::::::::.
 *           ``::::::::::::::::
 *             ::::``:::::::::'        .:::.
 *            ::::'   ':::::'       .::::::::.
 *         .::::'      ::::     .:::::::'::::.
 *        .:::'       :::::  .:::::::::' ':::::.
 *       .::'        :::::.:::::::::'      ':::::.
 *      .::'         ::::::::::::::'         ``::::.
 *  ...:::           ::::::::::::'              ``::.
 * ```` ':.          ':::::::::'                  ::::..
 *                   '.:::::'                    ':'````..
 * @version: V1.0
 * @author: qxw
 * @description: 获取用户名与密码详情接口
 * @data: 2020年04月11日 18:31
 * @ModifyDate: 2020年04月11日 18:31
 **/
public interface UsernamePasswordUserDetailService {

    /**
     * 验证用户名和密码
     * @param username 用户名
     * @param password 密码
     * @return 是否正确
     */
    boolean verifyCredential(String username, String password);

    /**
     * 获取用户详情
     * @param username 用户名
     * @return 用户详情
     */
    Object getUserDetail(String username);
}
