package com.awesome.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @author cdov
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    //重载configure(HttpSecurity)方法通过拦截器来保护请求
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //默认情况下,Java Configuration会启用CSRF保护.此时要禁用CSRF
        http.csrf().disable();
        //路由策略和访问权限的简单配置(允许/actuator/**健康检查请求下的请求可以访问，其他的都需要认证）
        http
                .authorizeRequests()
                .antMatchers("/actuator/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }
}
