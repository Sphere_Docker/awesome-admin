# 管理后台 V1.0.0

## 描述

本项目基于 Ant Design Pro V4.0 定制开发，加入以下内容:

1. 通过统一登录平台进行登录
2. 基于后台返回的 jwt token 中的角色进行权限认证
3. 增加 nodejs server 端 Koa、Koa router
4. 使用 dockerfile 进行部署

## 启动

1. 安装依赖

```bash
sudo npm install tyarn -g

```

或者

```
sudo npm install cnpm -g
npm install ms -S
cnpm install
目前采用的是这种方式，多次失败时，删除node-modules重新执行即可；
```

```
npm install -g cnpm --registry=https://registry.npm.taobao.org
```

2. 启动项目

```
npm start

注意：WebStorm下进入Terminal进入执行上述命令外，再在CMD下进入该项目录
cd E:\SpringCloud-Study-2020-02-25\awesome-admin\admin-ui
$ node ./server/app.js
```

## 编译项目

```bash
npm run build
```
